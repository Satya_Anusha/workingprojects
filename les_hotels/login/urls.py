from django.urls import path
#from .views import HomePageView
from . import views
urlpatterns = [
     path('',views.HomePageView.as_view(),name='home'),
     path('our choices/',views.our_choices,name='hotels'),
     path('packages/',views.packages,name='services'),
     path('about us/',views.about_us,name='about'),
     path('contact us/',views.contact_us,name='contact'),
     #path('our choices/home',views.our_choices_home,name='home'),
     #path('packages/home',views.packages_home,name='home'),
     #path('about us/home',views.about_us_home,name='home'),
     #path('contact us/home',views.contact_us_home,name='home'),
     #path('our choices/packages',views.our_choices_packages,name='services'),
     #path('our choices/about us',views.our_choices_about_us,name='about'),
     #path('our choices/contact us',views.our_choices_contact_us,name='contact'),
     #path('packages/our choices',views.our_choices_packages,name='hotels'),
     #path('packages/about us',views.our_choices_about_us,name='about'),
     #path('packages/contact us',views.our_choices_contact_us,name='contact'),
     #path('about us/our choices',views.our_choices_packages,name='hotels'),
     #path('about us/packages',views.our_choices_about_us,name='services'),
     #path('about us/contact us',views.our_choices_contact_us,name='contact'),
     #path('contact us/our choices',views.our_choices_packages,name='hotels'),
     #path('contact us/packages',views.our_choices_about_us,name='services'),
     #path('contact us/about us',views.our_choices_contact_us,name='about'),
     path('reco_hotel/',views.reco_hotel,name='hotel-room'),
     path('reco_hotel/',views.reco_hotel,name='hotel-room'),
     path('reco_hotel/',views.reco_hotel,name='hotel-room'),
]
