from django.db import models

# Create your models here.
class hotel(models.Model):
      hotel_name=models.CharField(max_length=250)
      location=models.CharField(max_length=250)
      cost=models.PositiveIntegerField()
      image=models.ImageField(upload_to='hol/',null=True) 
      
      def __str__(self):
          return self.hotel_name
