from django.shortcuts import render
from .models import hotel_list,category
# Create your views here.
def property_list(request):
    property_list=hotel_list.objects.all()
    template = 'list.html'
    search_query=request.GET.get('q')
    #cat_name=category.objects.all()
    #cat_name=cat_name.filter(category_name=search_query)
    #try:
    #    query=(search_query)
    #except:
    #    query=None
    #    property_list=None
    if search_query:
       print(search_query)
     #for i in cat_name:
       #property_list = property_list.filter(category=cat_name)
    context = {
         'property_list':property_list
}
    return render(request,template,context)
def property_detail(request ,id):
    property_detail=hotel_list.objects.get(id=id)
    template='detail.html'
    context={
         'property_detail':property_detail
}
    return render(request,template,context)
