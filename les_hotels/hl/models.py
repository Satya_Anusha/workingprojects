from django.db import models

# Create your models here.
class hotel_list(models.Model):
      hotel_name=models.CharField(max_length=250)
      category=models.ForeignKey('category',null=True,on_delete=models.SET_NULL)
      cost=models.PositiveIntegerField()
      image=models.ImageField(upload_to='hl/',null=True) 
      
      def __str__(self):
          return self.hotel_name
class category(models.Model):
      category_name=models.CharField(max_length=250)
      
      def __str__(self):
           return self.category_name


