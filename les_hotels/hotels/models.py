from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Hotels(models.Model):
      name = models.CharField(max_length=255)
      address = models.CharField(max_length=255)
      city = models.CharField(max_length=255)
      phone_number = models.CharField(max_length=12)
      image = models.ImageField(upload_to='hotels/',null=True)
      description = models.TextField(max_length=255)

      def __str__(self):
           return self.name
     
class review(models.Model):
      user = models.ForeignKey(User,on_delete=models.CASCADE)
      hotel = models.ForeignKey(Hotels,on_delete=models.CASCADE)
      rating = models.IntegerField(default = 0)
      
      def __str__(self):
           return self.comment
class Room(models.Model):
    hotel = models.ForeignKey(Hotels,on_delete=models.CASCADE)
    RoomType = models.CharField(max_length  = 255)
    Capacity = models.IntegerField(default = 0)
    Price= models.IntegerField(default = 0)
    View = models.CharField(max_length  = 255)
    TotalRooms = models.CharField(max_length  = 255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
         return self.RoomType
